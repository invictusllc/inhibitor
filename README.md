# Inhibitor &copy; 2022, Invictus808

## Overview

Inhibitor is a collection of all my tools and notes for engagements.


## Team

### Authors
- Blaise Aranador ([Invictus808](https://invictus808.com/))

### Maintainers
- Blaise Aranador ([Invictus808](https://invictus808.com/))


## Resources
- Cheatsheet ([S1REN](https://sirensecurity.io/blog/)) || [Cheatsheet](cheatsheets/)
- Machines ([HackTheBox](https://www.hackthebox.com/) || [VulnHub](https://www.vulnhub.com/)) || [List](engagements/machines_list.md)
- Engagement Notes ([CherryTree](https://www.kali.org/tools/cherrytree/)) || [Notes](engagements/Engagements.ctb)
- [TCM Security](https://tcm-sec.com)