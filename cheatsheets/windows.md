# Windows

## System Enumeration

```sh
getuid
```

```sh
systeminfo
```


### Windows Management Instrumentation (WMIC)

#### Quick Fix Engineering

```sh
wmic qfe
```

```sh
wmic qfe get Caption,Description,HotFixID,InstalledOn
```


#### Drives

```sh
wmic logicaldisk
```

```sh
wmic logicaldisk get caption,description,providername
```

```sh
wmic logicaldisk get caption
```


## User Enumeration

```sh
whoami
```

```sh
whoami /priv
```

```sh
whoami /groups
```

```sh
net user
```

```sh
net user $USER
```

```sh
net localgroup
```

```sh
net localgroup $GROUP
```


## Network Enumeration

```sh
ipconfig
```

```sh
ipconfig /all
```

```sh
arp -a
```

```sh
route print
```

```sh
netstat -ano
```

```sh
netsh wlan show profile
```

```sh
netsh wlan show profile $SSID key=clear
```


## Password Hunting

```sh
# $FILE can use wildcard (*)
findstr /si $STRING $FILE
```


### Resources

- [Sushant 747's Guide](https://sushant747.gitbooks.io/total-oscp-guide/content/privilege_escalation_windows.html)
- [PayloadsAllTheThings Guide](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Windows%20-%20Privilege%20Escalation.md)


## Antivirus (AV) Enumeration

### Service Control

```sh
sc query $SERVICE
```

```sh
sc queryex type=service
```


### Firewall

#### Newer Machines

```sh
netsh advfirewall firewall dump
```

```sh
netstat advfirewall firewall show rule all
```


#### Older Machines

```sh
netsh firewall show state
```

```sh
netsh firewall show config
```
