# HackTheBox: template
## Notes by [Invictus808](https://invictus808.com)

## Scan

- [nmap](enumeration/external/nmap.txt)
    - ```console
        $ nmap -T4 -p- -A $IP
        ```
- [nikto](enumeration/external/nikto.txt)
    - ```console
        $ nikto -h $URL
        ```
- gobuster
    - [dir](enumeration/external/gobuster-dir.txt)
        - ```console
            $ gobuster dir -u $URL -w /usr/share/wordlists/seclists/Discovery/Web-Content/raft-small-words.txt -x php,html -o gobuster-dir.txt
            ```
    - [vhost](enumeration/external/gobuster-vhost.txt)
        - ```console
            $ gobuster vhost --append-domain -t 50 -u $URL -w /usr/share/seclists/Discovery/DNS/subdomains-top1million-110000.txt -o gobuster-vhost.txt
            ```


## Dev

- ![Dev login page](screenshots/dev_login.png "Dev Login Page")


## Privilege Escalation

- notes


## Acknowledgment of Completion

- [HackTheBox - template Completion]()
