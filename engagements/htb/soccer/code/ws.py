import json
import sys
import _thread as thread
import time
import websocket

class WebSocket:
    def __init__(self, url):
        self.__url = url


    def on_message(self, ws, message):
        print(f"[*] < {message}")


    def on_error(self, ws, error):
        print(f"[-] Web Socket Error: {error}")


    def on_close(self, ws, close_status, close_message):
        if close_status:
            print(f"[*] {close_message} ({close_status})")
        else:
            print(f"[*] Connection closed.")


    def on_open(self, ws):
        def run(*args):
            while True:
                time.sleep(1)
                id = input("[*] > Enter \"id\" value (type \"q\" to quit): ")
                if id == "q":
                    break
                message = {"id": f"{id}"}
                ws.send(json.dumps(message))
            time.sleep(1)
            ws.close()
            print("[*] Thread terminating...")
        thread.start_new_thread(run, ())


    def connect(self):
        ws = websocket.WebSocketApp(self.__url, on_open=self.on_open, on_message=self.on_message, on_error=self.on_error, on_close=self.on_close)
        ws.run_forever()


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print(f"Error: Invalid amount of arguments. (Required: 1)")
        print(f"Syntax: python3 ws.py $WEB_SOCKET")
        sys.exit()

    w = WebSocket(sys.argv[1])
    w.connect()
