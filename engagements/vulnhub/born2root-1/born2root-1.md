# VulnHub: Born2Root 1

## OS:
## Web-Technology:

## IP: 10.8.8.104


## USERS:
- Secretsec Employees:
    - Martin N &rarr; martin@secretsec.com
    - Hadi M
    - Jimmy S


## CREDENTIALS (ANY):
- SSH
    - martin : <PRIVATE_KEY> &larr; enumeration
    - hadi : hadi123 &larr; bruteforce (cewl - generate wordlist, john generate password list, hydra - bruteforce login credentials)

---

## Community Attack Vectors (To-Try List):
- 80 HTTP &rarr; Files/Directories Enumeration (Web Root) &rarr; Manual Enumeration &rarr; robots.txt, .DS_Store, .svn
    - We found a private key from enumeration!
    - /icons/VDSoyuAXiO.txt
- 22 SSH &rarr; martin : <PRIVATE_KEY> &rarr;
```bash
ssh -i <PRIVATE_KEY_FILE> martin@$IP
```

---

## NMAP RESULTS:
```
22/tcp    open  ssh     OpenSSH 6.7p1 Debian 5+deb8u3 (protocol 2.0)
| ssh-hostkey: 
|   1024 3d:6f:40:88:76:6a:1d:a1:fd:91:0f:dc:86:b7:81:13 (DSA)
|   2048 eb:29:c0:cb:eb:9a:0b:52:e7:9c:c4:a6:67:dc:33:e1 (RSA)
|   256 d4:02:99:b0:e7:7d:40:18:64:df:3b:28:5b:9e:f9:07 (ECDSA)
|_  256 e9:c4:0c:6d:4b:15:4a:58:4f:69:cd:df:13:76:32:4e (ED25519)
80/tcp    open  http    Apache httpd 2.4.10 ((Debian))
| http-robots.txt: 2 disallowed entries 
|_/wordpress-blog /files
|_http-title:  Secretsec Company 
|_http-server-header: Apache/2.4.10 (Debian)
111/tcp   open  rpcbind 2-4 (RPC #100000)
| rpcinfo: 
|   program version    port/proto  service
|   100000  2,3,4        111/tcp   rpcbind
|   100000  2,3,4        111/udp   rpcbind
|   100000  3,4          111/tcp6  rpcbind
|   100000  3,4          111/udp6  rpcbind
|   100024  1          42039/udp   status
|   100024  1          46496/tcp6  status
|   100024  1          51286/tcp   status
|_  100024  1          58888/udp6  status
51286/tcp open  status  1 (RPC #100024)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

---

## Web Services Enumeration:

### [ + NIKTO ]


### [ + WFUZZ ]

#### FILES:
```
/ (Web Root)
None
```

```
/files
None
```

- /icons/VDSoyuAXiO.txt
```
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAoNgGGOyEpn/txphuS2pDA1i2nvRxn6s8DO58QcSsY+/Nm6wC
tprVUPb+fmkKvOf5ntACY7c/5fM4y83+UWPG0l90WrjdaTCPaGAHjEpZYKt0lEc0
FiQkXTvJS4faYHNah/mEvhldgTc59jeX4di0f660mJjF31SA9UgMLQReKd5GKtUx
5m+sQq6L+VyA2/6GD/T3qx35AT4argdk1NZ9ONmj1ZcIp0evVJvUul34zuJZ5mDv
DZuLRR6QpcMLJRGEFZ4qwkMZn7NavEmfX1Yka6mu9iwxkY6iT45YA1C4p7NEi5yI
/P6kDxMfCVELAUaU8fcPolkZ6xLdS6yyThZHHwIDAQABAoIBAAZ+clCTTA/E3n7E
LL/SvH3oGQd16xh9O2FyR4YIQMWQKwb7/OgOfEpWjpPf/dT+sK9eypnoDiZkmYhw
+rGii6Z2wCXhjN7wXPnj1qotXkpu4bgS3+F8+BLjlQ79ny2Busf+pQNf1syexDJS
sEkoDLGTBiubD3Ii4UoF7KfsozihdmQY5qud2c4iE0ioayo2m9XIDreJEB20Q5Ta
lV0G03unv/v7OK3g8dAQHrBR9MXuYiorcwxLAe+Gm1h4XanMKDYM5/jW4JO2ITAn
kPducC9chbM4NqB3ryNCD4YEgx8zWGDt0wjgyfnsF4fiYEI6tqAwWoB0tdqJFXAy
FlQJfYECgYEAz1bFCpGBCApF1k/oaQAyy5tir5NQpttCc0L2U1kiJWNmJSHk/tTX
4+ly0CBUzDkkedY1tVYK7TuH7/tOjh8M1BLa+g+Csb/OWLuMKmpoqyaejmoKkLnB
WVGkcdIulfsW7DWVMS/zA8ixJpt7bvY7Y142gkurxqjLMz5s/xT9geECgYEAxpfC
fGvogWRYUY07OLE/b7oMVOdBQsmlnaKVybuKf3RjeCYhbiRSzKz05NM/1Cqf359l
Wdznq4fkIvr6khliuj8GuCwv6wKn9+nViS18s1bG6Z5UJYSRJRpviCS+9BGShG1s
KOf1fAWNwRcn1UKtdQVvaLBX9kIwcmTBrl+e6P8CgYAtz24Zt6xaqmpjv6QKDxEq
C1rykAnx0+AKt3DVWYxB1oRrD+IYq85HfPzxHzOdK8LzaHDVb/1aDR0r2MqyfAnJ
kaDwPx0RSN++mzGM7ZXSuuWtcaCD+YbOxUsgGuBQIvodlnkwNPfsjhsV/KR5D85v
VhGVGEML0Z+T4ucSNQEOAQKBgQCHedfvUR3Xx0CIwbP4xNHlwiHPecMHcNBObS+J
4ypkMF37BOghXx4tCoA16fbNIhbWUsKtPwm79oQnaNeu+ypiq8RFt78orzMu6JIH
dsRvA2/Gx3/X6Eur6BDV61to3OP6+zqh3TuWU6OUadt+nHIANqj93e7jy9uI7jtC
XXDmuQKBgHZAE6GTq47k4sbFbWqldS79yhjjLloj0VUhValZyAP6XV8JTiAg9CYR
2o1pyGm7j7wfhIZNBP/wwJSC2/NLV6rQeH7Zj8nFv69RcRX56LrQZjFAWWsa/C43
rlJ7dOFH7OFQbGp51ub88M1VOiXR6/fU8OMOkXfi1KkETj/xp6t+
-----END RSA PRIVATE KEY-----
```

#### DIRECTORIES:
```
/ (Web Root)
=====================================================================
ID           Response   Lines    Word       Chars       Payload
=====================================================================

000000055:   200        15 L     49 W       737 Ch      "files"
000000345:   200        12 L     26 W       626 Ch      "manual"
000000386:   200        1024 L   4968 W     75937 Ch    "icons"
```

```
/files
None
```

```
/manual
lots of different language directories
```

---

## OTHER:
- Enumerate!
- /icons/VDSoyuAXiO.txt
    - Private key!
- ~/.ssh/config
```
Host *
PubkeyAcceptedKeyTypes=+ssh-rsa
```

- SSH using private key and employee names
    - martin?
    - hadi?
    - jimmy?
- martin works

- look in /home directories
- hadi has some interesting buffer overflow files...
    - gdb buff
    - p system
    - p exit
    - find $esp, +2000, “/bin/bash”
- generate exploit file
- find some way to load exploit into buff program?
    - system() = 0xb7e643e0
    - /bin/bash = 0xbffff908
    - exit() = 0xb7e571b0

- look in /etc/crontab
- /tmp/sekurity.py
    - owned by jimmy
    - lateral movement!
    - /tmp is world writable :D
- create reverse shell (python) 
- use nc to listen
- lateral movement to jimmy!
- but nothing...

- last user that has potential is hadi
- use cewl to generate possible words used in password by gathering information about machine
- use john the ripper ‘john’ to generate password list
- use hydra to test each password in list on user hadi using ssh method
- it worked!

---

## PRIV-ESC:

- Enumerate!! &larr; you found the private key :D
- Try all users found on the home page
- martin works! &larr; foothold
```bash
ssh -i born2root.key martin@$IP
```

- Enumerate directories
    - Anything look interesting?
    - Checkout home directories
        - martin
        - jimmy
        - hadi
    - Martin
        - nothing...
        - enumerate more !
        - /etc/crontab !!
        - runs python job (python /tmp/sekurity.py)
        - python job runs as user jimmy ← lateral movement
        - /tmp is world writable :D
        - REVERSE SHELL TIME !!
            - see python [reverse shell resource](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Reverse%20Shell%20Cheatsheet.md#python)
    - Jimmy
        - Reverse shelled \o/
        - networker executable
        - nothing interesting tho...
        - keep enumerating what's in hadi?
    - Hadi
        - buffer overflow files :o
        - kernel.randomize_va_space=2
        - Address Space Layout Randomization is disabled!!
        - This is good for a bufferoverflow attack payload
        - Attempted but no success yet...
- Bruteforce Passwords :/ &larr; another foothold
    - not my ideal method...
    - but we were able to get access to user hadi
    - hadi : hadi123
- Privilege Escalation
    - try su with hadi credentials?
    - TADAAA!! \o/

### PROOF
/root/flag.txt
```

,-----.                         ,---. ,------.                 ,--.   
|  |) /_  ,---. ,--.--.,--,--, '.-.  \|  .--. ' ,---.  ,---. ,-'  '-. 
|  .-.  \| .-. ||  .--'|      \ .-' .'|  '--'.'| .-. || .-. |'-.  .-' 
|  '--' /' '-' '|  |   |  ||  |/   '-.|  |\  \ ' '-' '' '-' '  |  |   
`------'  `---' `--'   `--''--''-----'`--' '--' `---'  `---'   `--'   


Congratulations ! you  pwned completly Born2root's CTF .

I hope you enjoyed it and you have made Tea's overdose or coffee's overdose :p 

I have blocked some easy ways to complete the CTF ( Kernel Exploit ... ) for give you more fun and more knownledge ...

Pwning the box with a linux binary misconfiguration is more fun than with a Kernel Exploit !

Enumeration is The Key .



Give me feedback :[FB] Hadi Mene
```

### RESULTS (Screenshot: Ctrl + Alt + Shift + R)
```bash
root@debian:~# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default 
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UNKNOWN group default qlen 1000
    link/ether 08:00:27:47:f7:fa brd ff:ff:ff:ff:ff:ff
    inet 10.8.8.104/24 brd 10.8.8.255 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe47:f7fa/64 scope link 
       valid_lft forever preferred_lft forever
root@debian:~# hostname
debian
root@debian:~# id
uid=0(root) gid=0(root) groupes=0(root)
root@debian:~# whoami
root
```

---

## Take Away Concepts:
- Good job with enumeration
- We found that private key
- Used
    - Cewl - generate a list of key phrases for bruteforce attack
    - John - generate a mutated list of passwords to use in bruteforce attack
    - Hydra - bruteforce attack ssh
- Found Credentials for Hadi
- su was our key to privilege escalation
- password for hadi was reused for root
