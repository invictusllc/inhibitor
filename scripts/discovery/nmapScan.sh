#!/bin/bash
# Credit: The Cyber Mentor, TCM Security

if [ "$1" == "" ]
then
    echo "Error: IP list"
    echo "Syntax: ./scan.sh ips.txt"
else
    for ip in $(cat $1); do
        nmap -p- -sC -sV $ip --open;
    done
fi
