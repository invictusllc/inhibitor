import logging
import argparse
import os

def parse_args():
    parser = argparse.ArgumentParser(description='Description of your program')
    parser.add_argument('--arg1', help='Description of arg1', required=True)
    parser.add_argument('--arg2', help='Description of arg2', required=True)
    return parser.parse_args()

def setup_logging():
    logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)-8s %(message)s')

def main():
    args = parse_args()
    setup_logging()

    # Your code goes here

if __name__ == '__main__':
    main()
