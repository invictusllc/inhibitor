#!/usr/bin/python3
'''
Copyright: Invictus808

Description: Convert SAM dump file to NT hash file

    SAM dump format: USER:ID:LM_HASH:NT_HASH:::
    Hash format: NT_HASH
'''
import sys


def main(file_path):
    '''
        :type file_path: str
        :rtype: None
    '''
    hashes = []
    with open(file_path, "r") as f:
        for line_number, line in enumerate(f.readlines(), start=1):
            account_details = line.split(":")
            if len(account_details) != 7:
                print(f"Error: Invalid SAM dump. (Line: {line_number})")
                print(f"Syntax: USER:ID:LM_HASH:NT_HASH:::")
                sys.exit()

            hashes.append(account_details[3])

    with open("./nt.hash", "w") as f:
        f.write("\n".join(hashes))


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print(f"Error: Invalid amount of arguments. (Required: 2)")
        print(f"Syntax: python3 samDump2hashFile.py /path/to/sam/dump")
        sys.exit()

    main(sys.argv[1])
