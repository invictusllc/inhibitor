#!/usr/bin/python3
'''
Copyright: Invictus808

Description: Merge SAM dump file with cracked hashes file

    SAM dump format: USER:ID:LM_HASH:NT_HASH:::
    Cracked hash format: NT_HASH:PASSWORD
    Merged format: USER:PASSWORD:ID:LM_HASH:NT_HASH:::
'''
from collections import defaultdict

import sys


def main(sam_dump_file_path, cracked_hashes_file_path):
    '''
        :type sam_dump_file_path: str
        :type cracked_hashes_file_path: str
        :rtype: None
    '''
    hash_lookup = defaultdict(list)
    with open(sam_dump_file_path, "r") as f:
        for line_number, line in enumerate(f.readlines(), start=1):
            account_details = line.split(":")
            if len(account_details) != 7:
                print(f"Error: Invalid SAM dump. (Line: {line_number})")
                print(f"Syntax: USER:ID:LM_HASH:NT_HASH:::")
                sys.exit()

            hash_lookup[account_details[3]].append(account_details)

    cracked = []
    with open(cracked_hashes_file_path) as f:
        for line_number, line in enumerate(f.readlines(), start=1):
            cracked_hash = line.split(":")
            if len(cracked_hash) != 2:
                print(f"Error: Invalid cracked hashes file. (Line: {line_number})")
                print(f"Syntax: HASH:PASSWORD")
                sys.exit()

            accounts = hash_lookup.get(cracked_hash[0])
            if not accounts:
                print(f"Error: Cracked hash is not in the SAM dump. (Hash: {cracked_hash[0]})")
                sys.exit()

            for account_details in accounts:
                account_details.insert(1, cracked_hash[1].strip())
                cracked.append(":".join(account_details))

    with open("./cracked.hash", "w") as f:
        f.write("".join(cracked))


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print(f"Error: Invalid amount of arguments. (Required: 3)")
        print(f"Syntax: python3 mergeSamDumpAndCrackedHashesFile.py /path/to/sam/dump /path/to/cracked/hashes")
        sys.exit()

    main(sys.argv[1], sys.argv[2])
